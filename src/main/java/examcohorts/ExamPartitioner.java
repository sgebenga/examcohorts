package examcohorts;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Partitioner;
import org.apache.hadoop.util.StringUtils;

public class ExamPartitioner extends Partitioner<DateWritable, DoubleWritable> {

    @Override
    public int getPartition(DateWritable dateWritable, DoubleWritable doubleWritable, int i) {
        int partition = 0;
        String date = dateWritable.getDate();
        if (date != null) {
            int year = getYear(date);
            partition = getPartition(year);
        }
        return partition;
    }

    private int getPartition(int year) {
        int partition = 0;
        if (year >= 1970 && year < 1990) {
            partition = 1;
        }
        return partition;
    }

    private int getYear(String date) {
        String[] fields = StringUtils.split(date, '/');
        return Integer.parseInt(fields[0]);
    }
}
