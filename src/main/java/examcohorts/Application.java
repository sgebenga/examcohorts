package examcohorts;

import com.sun.jersey.json.impl.provider.entity.JSONArrayProvider;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

public class Application extends Configured implements Tool {

    public static void main(String[] strings) throws Exception {
        int exitCode = ToolRunner.run(new Application(), strings);
        System.exit(exitCode);
    }

    public int run(String[] strings) throws Exception {
        Configuration configuration = getConf();
        configuration.set("mapreduce.output.textoutformat.separator", ",");

        Job job = Job.getInstance(configuration, " exam-cohorts application");
        job.setJarByClass(Application.class);

        FileInputFormat.addInputPaths(job, strings[0] + "," + strings[1]);
        Path out = new Path(strings[2]);

        FileSystem fsy = FileSystem.get(configuration);
        if (fsy.exists(out)) {
            fsy.delete(out, true);
        }

        FileOutputFormat.setOutputPath(job, out);

        job.setMapperClass(ExamMapper.class);
        job.setReducerClass(ExamReducer.class);
        job.setCombinerClass(ExamReducer.class);

        job.setOutputKeyClass(DateWritable.class);
        job.setOutputValueClass(DoubleWritable.class);

        job.setPartitionerClass(ExamPartitioner.class);
        return job.waitForCompletion(true) ? 0 : 1;
    }
}
