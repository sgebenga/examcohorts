package examcohorts;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.util.StringUtils;

import java.io.IOException;

public class ExamMapper extends Mapper<LongWritable, Text, DateWritable, DoubleWritable> {


    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] fields = StringUtils.split(value.toString(), StringUtils.COMMA);
        context.write(new DateWritable(fields[0]), new DoubleWritable(Double.parseDouble(fields[1])));
    }
}
