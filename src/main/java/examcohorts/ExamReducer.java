package examcohorts;

import org.apache.hadoop.io.DoubleWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

public class ExamReducer extends Reducer<DateWritable, DoubleWritable, DateWritable, DoubleWritable> {

    @Override
    protected void reduce(DateWritable key, Iterable<DoubleWritable> values, Context context) throws IOException, InterruptedException {
        double acum = 0;
        for (DoubleWritable value : values) {
            acum += value.get();
        }
        context.write(key, new DoubleWritable(acum));
    }

}
