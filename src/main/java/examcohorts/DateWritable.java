package examcohorts;

import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class DateWritable implements WritableComparable<DateWritable> {


    private String date;

    public DateWritable(String d) {
        this.date = d;
    }

    public DateWritable() {

    }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(date);
    }

    public void readFields(DataInput dataInput) throws IOException {
        date = dataInput.readUTF();
    }


    public int compareTo(DateWritable o) {
        return date.compareTo(o.date);
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String toString() {
        return date;
    }
}
